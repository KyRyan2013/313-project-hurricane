#board[x,y] contains [0]=piece owner, [1]=piece type, [2]=move pattern
#piece owner: 0=empty, 1=red, 2=blue
#
#
#
#
class gameboard:
    coordinates = dict()
    board = dict()
    __offset = 35
    redpattern=[(1,0)]    #not sure why you have (0,0) because that is not a movement, so deleted those
    bluepattern=[(-1,0)]
    #the above pattern code will soon be deprecated
    #the below code sets up rook and bishop patterns to be the same size as a standard chessboard, this may change later
    rookPattern=[]
    bishopPattern=[]
    for x in xrange(-8,9):
        if x!=0:
            rookPattern.append((x,0))
            rookPattern.append((0,x))
            bishopPattern.append((x,x))
            bishopPattern.append((-x,x))
    #What follows is set of dictionaries for different pieces based on player and piece type
    #{red/blue}Pieces["key"]= the following sequence[(xoffset1,yoffset1),...,(xoffsetk,yoffsetk)]
    redPieces=dict()
    redPieces["pawn"]=[(1,0)]
    redPieces["rook"]=rookPattern
    redPieces["knight"]=[(-1,-2),(-1,2),(1,-2),(1,2),(-2,-1),(-2,1),(2,-1),(2,1)]
    redPieces["bishop"]=bishopPattern
    redPieces["queen"]=rookPattern+bishopPattern
    redPieces["king"]=[(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
    bluePieces=dict()
    bluePieces["pawn"]=[(-1,0)]
    bluePieces["rook"]=rookPattern
    bluePieces["knight"]=[(-1,-2),(-1,2),(1,-2),(1,2),(-2,-1),(-2,1),(2,-1),(2,1)]
    bluePieces["bishop"]=bishopPattern
    bluePieces["queen"]=rookPattern+bishopPattern
    bluePieces["king"]=[(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
    #
    def __init__(self):
        for x in range(0, 20):
            for y in range(0, 10):
                xcoord = 75 + self.__offset*x
                ycoord = 75 + self.__offset*y
                self.coordinates[x,y] = xcoord,ycoord
                self.board[x,y] = 0,0,None
    def addPiece(self, xcoord, ycoord, owner, piecetype):
        if owner == 1:
            if piecetype ==1:
                self.board[xcoord, ycoord] = owner, piecetype, self.redpattern
            elif piecetype=="pawn":
                self.board[xcoord, ycoord] = owner, piecetype, self.redPieces["pawn"], 50
            elif piecetype=="rook":
                self.board[xcoord, ycoord] = owner, piecetype, self.redPieces["rook"], 200
            elif piecetype=="knight":
                self.board[xcoord, ycoord] = owner, piecetype, self.redPieces["knight"], 100
            elif piecetype=="bishop":
                self.board[xcoord, ycoord] = owner, piecetype, self.redPieces["bishop"], 250
            elif piecetype=="queen":
                self.board[xcoord, ycoord] = owner, piecetype, self.redPieces["queen"], 500
            elif piecetype=="king":
                self.board[xcoord, ycoord] = owner, piecetype, self.redPieces["king"], 1000
        elif owner == 2:
            if piecetype ==1:
                self.board[xcoord, ycoord] = owner, piecetype, self.bluepattern
            elif piecetype=="pawn":
                self.board[xcoord, ycoord] = owner, piecetype, self.bluePieces["pawn"], 50
            elif piecetype=="rook":
                self.board[xcoord, ycoord] = owner, piecetype, self.bluePieces["rook"], 200
            elif piecetype=="knight":
                self.board[xcoord, ycoord] = owner, piecetype, self.bluePieces["knight"], 100
            elif piecetype=="bishop":
                self.board[xcoord, ycoord] = owner, piecetype, self.bluePieces["bishop"], 250
            elif piecetype=="queen":
                self.board[xcoord, ycoord] = owner, piecetype, self.bluePieces["queen"], 500
            elif piecetype=="king":
                self.board[xcoord, ycoord] = owner, piecetype, self.bluePieces["king"], 1000
        elif owner == 0:
            self.board[xcoord, ycoord] = owner, piecetype, None
    def removePiece(self, xcoord, ycoord):
        self.board[xcoord, ycoord] = 0,0,None
    def movePiece(self, xcoord1, ycoord1, xcoord2, ycoord2):
        self.board[xcoord2, ycoord2] = self.board[xcoord1, ycoord1]
        self.board[xcoord1, ycoord1] = 0,0,None
    def getCoord(self, xcoord, ycoord):
        xval=-1
        yval=-1
        for x in range(0,20):
            for y in range(0,10):
                if(ycoord>self.coordinates[x,y][1] and ycoord<self.coordinates[x,y][1]+self.__offset):
                    yval=y
            if(xcoord>self.coordinates[x,y][0] and xcoord<self.coordinates[x,y][0]+self.__offset):
                xval=x
        return xval,yval
    def getPixelCoord(self, coord):
        trueval=self.coordinates[coord]
        return trueval
        
