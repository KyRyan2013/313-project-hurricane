import os
root = os.path.dirname(os.path.abspath(__file__))
import sys, pygame, gameboard, random, time
from pygame.locals import *
pygame.init()
screen = pygame.display.set_mode((900, 525), FULLSCREEN)

#board and background images
defaulttile = pygame.image.load(root+"\\images\\board_tile70X70.png").convert()
trenchtile = pygame.image.load(root+"\\images\\trench_tile70X70.png").convert()
movetile = pygame.image.load(root+"\\images\\movetile.png").convert()
background = pygame.image.load(root+"\\images\\backmap900x525.jpg").convert()
player1 = pygame.image.load(root+"\\images\\player1.jpg").convert()
player2 = pygame.image.load(root+"\\images\\player2.jpg").convert()

#yellow pieces images
yellowpawn = pygame.image.load(root+"\\images\\yellow_transparent.gif").convert()
yellowking = pygame.image.load(root+"\\images\\yking_transparent.gif").convert()
yellowqueen = pygame.image.load(root+"\\images\\yqueen_transparent.gif").convert()
yellowbishop = pygame.image.load(root+"\\images\\ybishop_transparent.gif").convert()
yellowknight = pygame.image.load(root+"\\images\\yknight_transparent.gif").convert()
yellowrook = pygame.image.load(root+"\\images\\yrook_transparent.gif").convert()
yellowbullet = pygame.image.load(root+"\\images\\ybullet_transparent.gif").convert()

#blue pieces images
bluepawn = pygame.image.load(root+"\\images\\blue_transparent.gif").convert()
blueking = pygame.image.load(root+"\\images\\bking_transparent.gif").convert()
bluequeen = pygame.image.load(root+"\\images\\bqueen_transparent.gif").convert()
bluebishop = pygame.image.load(root+"\\images\\bbishop_transparent.gif").convert()
blueknight = pygame.image.load(root+"\\images\\bknight_transparent.gif").convert()
bluerook = pygame.image.load(root+"\\images\\brook_transparent.gif").convert()
bluebullet = pygame.image.load(root+"\\images\\bbullet_transparent.gif").convert()

#explosion images
explosion1 = pygame.image.load(root+"\\images\\boom01_transparent.gif").convert()
explosion2 = pygame.image.load(root+"\\images\\boom02_transparent.gif").convert()
explosion3 = pygame.image.load(root+"\\images\\boom03_transparent.gif").convert()
explosion4 = pygame.image.load(root+"\\images\\boom04_transparent.gif").convert()
explosion5 = pygame.image.load(root+"\\images\\boom05_transparent.gif").convert()
explosion6 = pygame.image.load(root+"\\images\\boom06_transparent.gif").convert()
explosion7 = pygame.image.load(root+"\\images\\boom07_transparent.gif").convert()
explosion8 = pygame.image.load(root+"\\images\\boom08_transparent.gif").convert()
explosionList = [explosion1, explosion2, explosion3, explosion4, explosion5, explosion6, explosion7, explosion8]

#resources and points
redresources = 2000 #starting resources
blueresources = 2000
#starting points
redpoints = 0
bluepoints=0

missFont = pygame.font.Font(None,15)


gameFont = pygame.font.Font(None, 30)
redPointsText = gameFont.render('Player 1 Score: ' + str(redpoints), 2, [255,0,0])
redResourceText = gameFont.render('Player 1 Resources: ' + str(redresources), 2, [255,0,0])

boxWidth=redPointsText.get_rect()
redPointsXpos = 40
redResourceXpos = 40


bluePointsText = gameFont.render('Player 2 Score: ' + str(bluepoints), 2, [0,255,0])
blueResourceText = gameFont.render('Player 2 Resources: ' + str(blueresources), 2, [0,255,0])
box2Width = bluePointsText.get_rect()
bluePointsXpos = 625
blueResourceXpos = 625

#player turn
playerTurn = 1
playerTurnText = gameFont.render('Player Turn: ' + str(playerTurn), 2, [50,50,255])
boxPlayerTurnWidth = playerTurnText.get_rect()
playerTurnXpos = 350

coordinates = list()
currpos = [0,0]
board = gameboard.gameboard()
piecetype = "pawn"
turn = 1
phase = 1 #1 is move; 2 is cannon

#add the default pieces to the board for testing
board.addPiece(1,4,1,"king")
board.addPiece(1,5,1,"queen")
board.addPiece(1,6,1,'bishop')
board.addPiece(1,7,1,'knight')
board.addPiece(1,8,1,'rook')
board.addPiece(18,4,2,"king")
board.addPiece(18,5,2,"queen")
board.addPiece(18,6,2,'bishop')
board.addPiece(18,7,2,'knight')
board.addPiece(18,8,2,'rook')

#update screen method, more comments coming
def updateScreen(selected, coord):
    screen.blit(background, (0,0))
    for y in xrange(0, 5):
        for x in xrange(0, 10):
            xcoord = 75 + 70*x
            ycoord = 75 + 70*y
            if(x>0 and x<9):
                screen.blit(defaulttile, (xcoord, ycoord))
            else: screen.blit(trenchtile, (xcoord, ycoord))
    if selected:
        movepat = board.board[coord][2]
        for m in movepat:
            movecoord = [-1,-1]
            movecoord[0] = coord[0] + m[0]
            movecoord[1] = coord[1] + m[1]
            if (movecoord[0]>-1 and movecoord[0]<20 and movecoord[1]>-1 and movecoord[1]<10):
                screen.blit(movetile, ((75+35*movecoord[0]),(75+35*movecoord[1])))
    screen.blit(yellowking, [40,480])
    screen.blit(yellowqueen, [75,480])
    screen.blit(yellowbishop, [110, 480])
    screen.blit(yellowknight, [145, 480])
    screen.blit(yellowrook, [180, 480])
    screen.blit(yellowpawn, [215, 480])
    screen.blit(blueking, [625, 480])
    screen.blit(bluequeen, [660,480])
    screen.blit(bluebishop, [695,480])
    screen.blit(blueknight, [730,480])
    screen.blit(bluerook, [765,480])
    screen.blit(bluepawn, [800,480])
    for y in xrange(0,10):
        for x in xrange(0, 20):
            if (board.board[x,y][0] == 1):
                playerTurnText = gameFont.render('Player Turn: ' + str(playerTurn), 2, [50,50,255])
                redPointsText = gameFont.render('Player 1 Points: ' + str(redpoints), 2, [255,0,0])
                redResourceText = gameFont.render('Player 1 Resources: ' + str(redresources), 2, [255,0,0])
                screen.blit(redPointsText, [redPointsXpos,440])
                screen.blit(redResourceText, [redResourceXpos, 460])
                screen.blit(playerTurnText, [playerTurnXpos, 30])
                if board.board[x,y][1] == 'king':
                    screen.blit(yellowking, board.coordinates[x,y])
                elif board.board[x,y][1] == 'queen':
                    screen.blit(yellowqueen, board.coordinates[x,y])
                elif board.board[x,y][1] == 'bishop':
                    screen.blit(yellowbishop, board.coordinates[x,y])
                elif board.board[x,y][1] == 'knight':
                    screen.blit(yellowknight, board.coordinates[x,y])
                elif board.board[x,y][1] == 'rook':
                    screen.blit(yellowrook, board.coordinates[x,y])
                else:
                    screen.blit(yellowpawn, board.coordinates[x,y])
            elif (board.board[x,y][0] == 2):
                bluePointsText = gameFont.render('Player 2 Points: ' + str(bluepoints), 2, [0,0,255])
                blueResourceText = gameFont.render('Player 2 Resources: ' + str(blueresources), 2, [0,0,255])
                screen.blit(bluePointsText, [bluePointsXpos, 440])
                screen.blit(blueResourceText, [blueResourceXpos, 460])
                if board.board[x,y][1] == 'king':
                    screen.blit(blueking, board.coordinates[x,y])
                elif board.board[x,y][1] == 'queen':
                    screen.blit(bluequeen, board.coordinates[x,y])
                elif board.board[x,y][1] == 'bishop':
                    screen.blit(bluebishop, board.coordinates[x,y])
                elif board.board[x,y][1] == 'knight':
                    screen.blit(blueknight, board.coordinates[x,y])
                elif board.board[x,y][1] == 'rook':
                    screen.blit(bluerook, board.coordinates[x,y])
                else:
                    screen.blit(bluepawn, board.coordinates[x,y])

#shoot method
def shooty(coord1, coord2):
    currpos=board.coordinates[coord1]
    endpos=board.coordinates[coord2]
    while currpos[0] < endpos[0]:
        updateScreen(False, 0)
        screen.blit(yellowbullet, currpos)
        pygame.display.flip()
        temporaryxvalue = currpos[0]
        temporaryxvalue = temporaryxvalue+3
        currpos= temporaryxvalue, currpos[1]
            
#shoot method for blue
def shootb(coord1, coord2):
    currpos=board.coordinates[coord1]
    endpos=board.coordinates[coord2]
    while currpos[0] > endpos[0]:
        updateScreen(False, 0)
        screen.blit(bluebullet, currpos)
        pygame.display.flip()
        temporaryxvalue = currpos[0]
        temporaryxvalue = temporaryxvalue-3
        currpos=temporaryxvalue, currpos[1]
        
#hit method
def hit(coordx, coordy):
    for e in explosionList:
        updateScreen(False, 0)
        screen.blit(e, board.coordinates[coordx,coordy])
        pygame.display.flip()
        time.sleep(0.075)
            
    
#we update the screen once to render everything for the first time
updateScreen(False, 0)
pygame.display.flip()
while True:
    pygame.event.pump()
    event = pygame.event.wait()
    if event.type==QUIT:
        pygame.display.quit()
        sys.exit()
    elif event.type==pygame.KEYDOWN:
        key = pygame.key.get_pressed()
        if key[pygame.K_ESCAPE]:
            pygame.display.quit()
            sys.exit()
        elif key[pygame.K_b]:
            piecetype = "bishop"
        elif key[pygame.K_c]:
            piecetype = "king"
        elif key[pygame.K_k]:
            piecetype = "knight"
        elif key[pygame.K_p]:
            piecetype = "pawn"
        elif key[pygame.K_q]:
            piecetype = "queen"
        elif key[pygame.K_r]:
            piecetype = "rook"
    elif event.type==pygame.MOUSEBUTTONUP:
        x, y = pygame.mouse.get_pos()
        mousecoord=board.getCoord(x,y)
        try:
            #if(mousecoord[0]<2 and board.board[mousecoord][1] == 0):
            #    board.addPiece(mousecoord[0], mousecoord[1], 1, piecetype)
            #elif(mousecoord[0]>17 and board.board[mousecoord][1] == 0):
            #    board.addPiece(mousecoord[0], mousecoord[1], 2, piecetype)

            #Piece buy phase
            if x > 40 and x < 250 and y > 480 and y < 515 and turn == 1:
                for tempx in range(6):
                    print "GOT THE ZERO HIT"
                    if x > (40+tempx*35) and x < (40+(tempx+1)*35):
                        print "GOT THE FIRST HIT"
                        repeat = True
                        while repeat:
                            event2 = pygame.event.wait()
                            if event2.type == pygame.MOUSEBUTTONUP:
                                print "GOT THE SECOND HIT"
                                repeat=False
                                x2,y2 = pygame.mouse.get_pos()
                                mousecoord2 = board.getCoord(x2,y2)
                                if(mousecoord2[0]<2 and board.board[mousecoord2][1]==0):
                                    if tempx == 0 and redresources>=1000:
                                        redresources=redresources-1000
                                        board.addPiece(mousecoord2[0],mousecoord2[1],1,'king')
                                    elif tempx == 1 and redresources>=500:
                                        redresources = redresources-500
                                        board.addPiece(mousecoord2[0],mousecoord2[1],1,'queen')
                                    elif tempx == 2 and redresources>=250:
                                        redresources = redresources-250
                                        board.addPiece(mousecoord2[0],mousecoord2[1],1,'bishop')
                                    elif tempx == 3 and redresources>=100:
                                        redresources = redresources-100
                                        board.addPiece(mousecoord2[0],mousecoord2[1],1,'knight')
                                    elif tempx == 4 and redresources>=200:
                                        redresources = redresources-200
                                        board.addPiece(mousecoord2[0],mousecoord2[1],1,'rook')
                                    elif tempx == 5 and redresources>=50:
                                        redresources = redresources-50
                                        board.addPiece(mousecoord2[0],mousecoord2[1],1,'pawn')
            elif x > 625 and x < 835 and y > 480 and y < 515 and turn == 2:
                for tempx in range(6):
                    if x > (625+tempx*35) and x < (625+(tempx+1)*35):
                        repeat = True
                        while repeat:
                            event2 = pygame.event.wait()
                            if event2.type == pygame.MOUSEBUTTONUP:
                                repeat = False
                                x2,y2 = pygame.mouse.get_pos()
                                mousecoord2 = board.getCoord(x2,y2)
                                if(mousecoord2[0]>17 and board.board[mousecoord2][1]==0):
                                    if tempx == 0 and blueresources>=1000:
                                        blueresources -= 1000
                                        board.addPiece(mousecoord2[0],mousecoord2[1],2,'king')
                                    elif tempx == 1 and blueresources>=500:
                                        blueresources -= 500
                                        board.addPiece(mousecoord2[0],mousecoord2[1],2,'queen')
                                    elif tempx == 2 and blueresources>=250:
                                        blueresources -= 250
                                        board.addPiece(mousecoord2[0],mousecoord2[1],2,'bishop')
                                    elif tempx == 3 and blueresources>=100:
                                        blueresources -= 100
                                        board.addPiece(mousecoord2[0],mousecoord2[1],2,'knight')
                                    elif tempx == 4 and blueresources>=200:
                                        blueresources -= 200
                                        board.addPiece(mousecoord2[0],mousecoord2[1],2,'rook')
                                    elif tempx == 5 and blueresources>=50:
                                        blueresources -= 50
                                        board.addPiece(mousecoord2[0],mousecoord2[1],2,'pawn')
            #move phase
            elif((board.board[mousecoord][0] == 1 and turn ==1 and phase == 1) or (board.board[mousecoord][0]==2 and turn == 2 and phase == 1)):
                repeat = True
                while repeat:
                    updateScreen(True, mousecoord)
                    pygame.display.flip()
                    event2 = pygame.event.wait()
                    if event2.type==QUIT:
                        pygame.display.quit()
                        sys.exit()
                    elif event2.type==pygame.KEYDOWN:
                        key = pygame.key.get_pressed()
                        if key[pygame.K_ESCAPE]:
                            pygame.display.quit()
                            sys.exit()
                    elif event2.type==pygame.MOUSEBUTTONUP:
                        repeat = False
                        x2, y2 = pygame.mouse.get_pos()
                        mousecoord2 = board.getCoord(x2,y2)
                        movepat = board.board[mousecoord][2]
                        for m in movepat:
                            if (mousecoord[0]+m[0])==mousecoord2[0] and (mousecoord[1]+m[1])==mousecoord2[1]:
                                tempmove=mousecoord[0]+m[0], mousecoord[1]+m[1]
                                if(turn == 1 and board.board[tempmove][0]!=1)or(turn == 2 and board.board[tempmove][0]!=2):
                                    if(turn == 1 and board.board[tempmove][0]==2):
                                        redpoints += 1
                                        redresources += (board.board[tempmove][3])/2
                                        #resources...points
                                    elif(turn == 2 and board.board[tempmove][0]==1):
                                        bluepoints += 1
                                        blueresources += (board.board[tempmove][3])/2
                                        
                                    board.movePiece(mousecoord[0], mousecoord[1], mousecoord2[0], mousecoord2[1])
                                    phase = 2
                                    
                                break
            #section for handling shoot phase
            #Player 1 shoot phase
            elif (board.board[mousecoord][0] == 2 and turn == 1 and phase == 2):
                distance = mousecoord[0] - 1
                print "mousecoord"
                print mousecoord[0]
                print distance
                if distance < 1:
                    distance = 0    #auto hit
                chance = random.randint(1,distance+1)
                if distance > 16:   #in trench still
                    chance = 0      #shot will miss
                shooty((2,mousecoord[1]), mousecoord)
                if chance == 1:
                    #hit
                    hit(mousecoord[0], mousecoord[1])
                    redresources += (board.board[mousecoord[0], mousecoord[1]][3])/2
                    redpoints += 1
                    board.removePiece(mousecoord[0], mousecoord[1])
                else:
                    #miss
                    updateScreen(False,0)
                    missText = missFont.render('MISS', 2, [255,255,255])
                    screen.blit(missText,[x,y])
                    pygame.display.flip()
                    time.sleep(1)
                turn = 2
                playerTurn = 2
                phase = 1
            #player 2 shoot phase
            elif (board.board[mousecoord][0] == 1 and turn == 2 and phase == 2):
                distance = 18 - mousecoord[0]
                print distance
                if distance < 1:
                    distance = 0
                chance = random.randint(1,distance+1)
                if distance > 16:
                    chance = 0
                shootb((17,mousecoord[1]), mousecoord)
                if chance == 1:
                    #hit
                    hit(mousecoord[0],mousecoord[1])
                    blueresources += (board.board[mousecoord[0], mousecoord[1]][3])/2
                    bluepoints += 1
                    board.removePiece(mousecoord[0],mousecoord[1])
                else:
                    #miss
                    updateScreen(False, 0)
                    missText = missFont.render('MISS', 2, [255,255,255])
                    screen.blit(missText,[x,y])
                    pygame.display.flip()
                    time.sleep(1)
                turn = 1
                phase = 1
                playerTurn = 1
        except Exception as detail:
            print "error", detail
        updateScreen(False, 0)
        pygame.display.flip()
